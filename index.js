let http = require("http");

http.createServer(function (request, response) {

	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your Profile.');
	}

	if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Here are our available courses.');
	}

	if(request.url == "/addcourse" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Add a course to our resources.');
	}

	if(request.url == "/updatecourse" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Update a course to our resources.');
	}

	if(request.url == "/archivecourses" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Archive courses to our resources.');
	}

	if(request.method == "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Welcome to the Booking system.')
	}
}).listen(4000);

console.log('server running at localhost:4000');